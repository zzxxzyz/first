class Post < ActiveRecord::Base
  has_many :comments, dependent: :destroy
  valdates_presence_of :title
  valdates_presence_of :body
end
